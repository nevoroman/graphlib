package com.nevoroman.graphlib.exceptions;

public class GraphlibException extends RuntimeException {
    public GraphlibException() {}
    public GraphlibException(String message) { super(message); }
}
