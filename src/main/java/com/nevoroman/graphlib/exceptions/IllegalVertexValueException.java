package com.nevoroman.graphlib.exceptions;

public class IllegalVertexValueException extends GraphlibException {
    public IllegalVertexValueException(String message) {
        super(message);
    }
}
