package com.nevoroman.graphlib;

import static com.nevoroman.graphlib.Edge.edge;

public class DirectedGraph<T> extends Graph<T> {

    @Override
    public Edge<T> addEdge(Edge<T> edge) {
        assertVertexExists(edge.source);
        assertVertexExists(edge.target);

        edges.get(edge.source).add(edge);
        return edge;
    }

    @Override
    public void removeEdge(T source, T target) {
        assertVertexExists(source);
        assertVertexExists(target);

        edges.get(source).remove(edge(source, target));
    }

}
