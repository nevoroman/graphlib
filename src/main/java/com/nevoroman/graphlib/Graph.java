package com.nevoroman.graphlib;

import static com.nevoroman.graphlib.Edge.edge;
import static java.util.Collections.unmodifiableSet;

import com.nevoroman.graphlib.exceptions.IllegalVertexValueException;
import com.nevoroman.graphlib.exceptions.VertexAlreadyExistsException;
import com.nevoroman.graphlib.exceptions.VertexNotFoundException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class Graph<T> {
    protected final Map<T, Set<Edge<T>>> edges;

    public Graph() {
        edges = new HashMap<>();
    }

    public abstract Edge<T> addEdge(Edge<T> edge);
    public abstract void removeEdge(T from, T to);

    public T addVertex(T value) {
        if (value == null) throw new IllegalVertexValueException("Vertex value should not be null");
        if (edges.containsKey(value)) throw new VertexAlreadyExistsException();

        edges.put(value, new HashSet<>());
        return value;
    }

    public void removeVertex(T value) {
        edges.values().forEach(
            es -> es.removeIf(e -> e.source == value || e.target == value)
        );
        edges.remove(value);
    }

    public Set<T> vertexSet() {
        return unmodifiableSet(edges.keySet());
    }

    public Set<Edge<T>> vertexEdges(T vertex) {
        assertVertexExists(vertex);
        return Collections.unmodifiableSet(edges.get(vertex));
    }

    public List<T> vertexNeighbors(T vertex) {
        assertVertexExists(vertex);
        return edges.get(vertex).stream().map(x -> x.target).collect(Collectors.toList());
    }

    public Optional<List<Edge<T>>> getPath(T start, T destination) {
        if (start.equals(destination)) throw new IllegalVertexValueException("Couldn't build path from vertex to itself");

        final var paths = new ArrayDeque<>(List.of(List.of(start)));
        final var visited = new HashSet<T>();

        List<T> path;
        while (!paths.isEmpty()) {
            path = paths.remove();
            final var lastVertex = path.get(path.size() - 1);

            for (var neighbor: vertexNeighbors(lastVertex)) {
                var newPath = new ArrayList<>(path);
                newPath.add(neighbor);
                if (neighbor.equals(destination)) {
                    return Optional.of(buildPath(newPath));
                }
                else if (!visited.contains(neighbor)) {
                    visited.add(neighbor);
                    paths.add(newPath);
                }
            }
        }

        return Optional.empty();
    }

    protected void assertVertexExists(T vertex) {
        if (!edges.containsKey(vertex)) throw new VertexNotFoundException();
    }

    private List<Edge<T>> buildPath(List<T> vertexes) {
        var result = new ArrayList<Edge<T>>();
        for (int i = 1; i < vertexes.size(); i++) {
            result.add(edge(vertexes.get(i - 1), vertexes.get(i)));
        }
        return result;
    }

}
