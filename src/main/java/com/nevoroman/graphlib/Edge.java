package com.nevoroman.graphlib;

import com.nevoroman.graphlib.exceptions.IllegalVertexValueException;
import java.util.Objects;

public class Edge<T> {
    public final T source;
    public final T target;

    private Edge(T source, T target) {
        if (source == null || target == null) throw new IllegalVertexValueException("Vertex value could not be null");
        this.source = source;
        this.target = target;
    }

    public static <T> Edge<T> edge(T source, T target) {
        return new Edge<>(source, target);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Edge<?> edge = (Edge<?>) o;
        return source.equals(edge.source) &&
            target.equals(edge.target);
    }

    @Override
    public int hashCode() {
        return Objects.hash(source, target);
    }

    @Override
    public String toString() {
        return String.format("[%s - %s]", source.toString(), target.toString());
    }
}
