package com.nevoroman.graphlib;

import static com.nevoroman.graphlib.Edge.edge;

public final class UndirectedGraph<T> extends Graph<T> {

    @Override
    public Edge<T> addEdge(Edge<T> edge) {
        assertVertexExists(edge.source);
        assertVertexExists(edge.target);

        edges.get(edge.source).add(edge);
        edges.get(edge.target).add(edge(edge.target, edge.source));
        return edge;
    }

    @Override
    public void removeEdge(T v1, T v2) {
        assertVertexExists(v1);
        assertVertexExists(v2);

        edges.get(v1).remove(edge(v1, v2));
        edges.get(v2).remove(edge(v2, v1));
    }

}
