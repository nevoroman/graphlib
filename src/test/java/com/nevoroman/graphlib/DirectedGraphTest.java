package com.nevoroman.graphlib;

import static com.nevoroman.graphlib.Edge.edge;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import com.nevoroman.graphlib.exceptions.IllegalVertexValueException;
import com.nevoroman.graphlib.exceptions.VertexNotFoundException;
import java.util.List;
import org.junit.jupiter.api.Test;

public class DirectedGraphTest {

    @Test
    void addVertexShouldCorrectlyAddNewVertex() {
        // given
        var graph = new DirectedGraph<String>();

        // when
        var vertex = graph.addVertex("newVertex");

        // then
        assertThat(vertex).isEqualTo("newVertex");
        assertThat(graph.vertexSet()).containsOnly(vertex);
    }

    @Test
    void addVertexShouldFailOnNullValue() {
        // given
        var graph = new DirectedGraph<String>();

        // when // then
        assertThatThrownBy(() -> graph.addVertex(null))
            .isExactlyInstanceOf(IllegalVertexValueException.class)
            .hasMessage("Vertex value should not be null");
    }

    @Test
    void removeVertexShouldRemoveExistingVertexAndRelatedEdges() {
        // given
        var graph = new DirectedGraph<String>();
        graph.addVertex("first");
        graph.addVertex("second");
        graph.addVertex("third");
        graph.addEdge(edge("first", "second"));
        graph.addEdge(edge("second", "third"));
        graph.addEdge(edge("first", "third"));
        graph.addEdge(edge("third", "second"));

        // when
        graph.removeVertex("second");

        // then
        assertThat(graph.vertexSet()).containsOnly("first", "third");
        assertThat(graph.vertexEdges("first")).containsOnly(edge("first", "third"));
        assertThat(graph.vertexEdges("third")).isEmpty();
    }

    @Test
    void addEdgeShouldCorrectlyAddNewEdge() {
        // given
        var graph = new DirectedGraph<String>();
        graph.addVertex("firstVertex");
        graph.addVertex("secondVertex");

        // when
        var edge = graph.addEdge(edge("firstVertex", "secondVertex"));

        // then
        assertThat(graph.vertexEdges("firstVertex")).containsOnly(edge);
        assertThat(graph.vertexEdges("secondVertex")).isEmpty();
    }

    @Test
    void addEdgeShouldFailIfVertexNotExists() {
        // given
        var graph = new DirectedGraph<String>();
        graph.addVertex("firstVertex");
        graph.addVertex("wrongVertex");

        // when // then
        assertThatThrownBy(() -> graph.addEdge(edge("firstVertex", "secondVertex")))
            .isExactlyInstanceOf(VertexNotFoundException.class);
    }

    @Test
    void removeEdgeShouldRemoveExistingEdge() {
        // given
        var graph = new DirectedGraph<String>();
        graph.addVertex("first");
        graph.addVertex("second");
        graph.addVertex("third");
        graph.addEdge(edge("first", "second"));
        graph.addEdge(edge("first", "third"));

        // when
        graph.removeEdge("first", "second");

        // then
        assertThat(graph.vertexEdges("first")).containsOnly(edge("first", "third"));
    }

    @Test
    void removeEdgeShouldFailIfNoVertexFound() {
        // given
        var graph = new DirectedGraph<String>();
        graph.addVertex("first");

        // when // then
        assertThatThrownBy(() -> graph.removeEdge("first", "random"))
            .isExactlyInstanceOf(VertexNotFoundException.class);
    }

    @Test
    void getPathShouldReturnCorrectPathBetweenEdges() {
        // given
        var graph = new DirectedGraph<String>();
        graph.addVertex("first");
        graph.addVertex("second");
        graph.addVertex("third");
        graph.addVertex("forth");
        graph.addEdge(edge("first", "second"));
        graph.addEdge(edge("first", "third"));
        graph.addEdge(edge("third", "forth"));
        graph.addEdge(edge("third", "second"));

        // when
        var result = graph.getPath("first", "forth");

        // then
        var expectedPath = List.of(edge("first", "third"), edge("third", "forth"));
        assertThat(result).contains(expectedPath);
    }

    @Test
    void getPathShouldReturnEmptyIfNoPathFound() {
        // given
        var graph = new DirectedGraph<String>();
        graph.addVertex("first");
        graph.addVertex("second");
        graph.addVertex("third");
        graph.addVertex("forth");
        graph.addEdge(edge("first", "second"));
        graph.addEdge(edge("first", "third"));
        graph.addEdge(edge("third", "second"));
        graph.addEdge(edge("forth", "third"));


        // when
        var path = graph.getPath("first", "forth");

        // then
        assertThat(path).isEmpty();
    }

    @Test
    void getPathShouldFailIfVertexNotExists() {
        // given
        var graph = new DirectedGraph<String>();
        graph.addVertex("first");

        // when
        assertThatThrownBy(() -> graph.getPath("random", "first"))
            .isExactlyInstanceOf(VertexNotFoundException.class);
    }

    @Test
    void getPathShouldFailIfTargetEqualsToDestination() {
        // given
        var graph = new DirectedGraph<String>();
        graph.addVertex("first");

        // when
        assertThatThrownBy(() -> graph.getPath("first", "first"))
            .isExactlyInstanceOf(IllegalVertexValueException.class)
            .hasMessage("Couldn't build path from vertex to itself");
    }

}
